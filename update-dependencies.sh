#!/bin/bash
set -euo pipefail
IFS=$'\n\t'


echo "Docker login to registry.gitlab.com"
docker login registry.gitlab.com

echo "Specify new docker tag: "
read -r TAGVERSION

IMAGE_NAME=registry.gitlab.com/marcus.held/code-held:$TAGVERSION

docker-compose run blog bundle update
docker build -t $IMAGE_NAME .
docker push $IMAGE_NAME

echo "Removing cache"
rm -rf src/.jekyll-cache src/_site

echo "Done, replace $IMAGE_NAME in docker-compose.yml"
