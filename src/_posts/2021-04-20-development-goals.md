---
layout: post 
title: Some Thoughts about Personal Development Goals for Software Engineers
image: assets/images/posts/2021/04/development-goals.jpg
image-alt: A typewriter with a letter with the word "goal"
tags: [Leadership]
highlight: false 
call-to-action: Did this post help you? Tell me your experience 
credits: Photo by <a href="https://unsplash.com/@markuswinkler?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Markus Winkler</a> on <a href="https://unsplash.com/s/photos/goal?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
---
Personal development goals in software engineering are not trivial to define. In general, goals should be SMART (Specific, Measurable, Achievable, Realistic and Timely). But in reality our field of expertise is that diverse and complex that it is not as easy to find those goals like in other professions. For someone working in the sales department a revenue goal is easy to define and to decide whether he succeeded or not. For us software developers it is different. In order to improve we need abstract goals that are independent enough to survive in our agile day-to-day business. I received many goals in the past, and a lot of them felt wrong, unfair and not helpful for my personal development. For my employees I took another approach. The overall goal needs to be to enable your colleagues and in this post I describe how I achieve that.

## A Story

In the past I had a goal that read like this:
> citation ""
> &lt;Employee> will make sure that the &lt;insert feature here> is implemented on the highest quality standard within two sprints.

What were the problems with this kind of goal?  
First I worked in an agile environment. As a software developer I had zero chance to influence when and, if this feature gets implemented at all. It actually happened that the product owner decided that this feature was out of scope for the current state of the project and therefore I never had the chance to even touch it. But did I fail this goal now? Objectively, I did. I did not make sure that the feature got implemented. But would it have made sense to fight for it? No! Of course not! The overall goal of me and the company was not to achieve my personal development goals but to launch the product successfully to the market. Being measured by such a specific goal felt unfair and unachievable. The situation got worse when my career was actually measured by looking at how many goals I achieved in the past years.

Secondly, what means _the highest quality_? This goal is not measurable at all. As we will see later this is something I think that is unsolvable for our profession, but what caused the problem here was that we only talked about it after a year when we reflected if I succeeded or not. Being in that growth talk where you had to argue if you succeeded or not and whereas the result of that discussion had direct influence on my raise was super stressful. For me as well as for my supervisor. It did not help to achieve the goal of helping me to improve in my skills.

Lastly _within two sprints_ is too specific again. At the point in time when these goals were defined the overall scope of the feature wasn't even clear and of course not estimated at all. So why this arbitrary time limitation? It does not make sense and causes the same issues as described earlier.

Overall one thing we didn't even discuss until this point. Does the content of this goal even succeed in developing my skills to improve as a software developer? No. Not at all. The goal that was defined here was just a description of my job profile. It is describing what I (have to) do every day. I develop a feature in the best quality I'm able to deliver. Nothing more. This is not a good goal to have in the first place.

## A Goal Should Push into Cold Water

So, let's take a step back and think about what we want to achieve with personal development goals. We want to change a behavior. We want to force our employees to step into terrain which they didn't discover yet. Something new that we want them to learn. Something where they can actually develop and discover a skill they were not aware of before.

One goal, for a young colleague, that I defined was:

> citation ""
> In the next year, &lt;employee> will take the lead on at least one topic, work it out and, among other things, implement it. In doing so, &lt;employee> will independently identify the tasks to be completed and break them down into small, lockable chunks.

The colleague did mainly work on a fixed and very stable project in the past year. In this project the preparation of topics, and the resulting stories are very well-prepared. He "just" has to do it. With this goal I force him to leave this safe zone and take more responsibility in a controlled environment.

## Speak About the Goals. Often!

You might interject that the goal above is very vague and can cause similar issues like I had described earlier. Indeed, when you just pull out this goal in the next growth talk a year later you and your employee most likely have very different assumption whether this goal was achieved. You don't want this kind of surprise. This is not helpful for what you actually want to achieve - helping the employee to gain more experience and skills.

It is important that you reflect the goals regular with your employees. Once a month, every second month. This should be the timeframe where you take the time to read over the goals, take notes on the progress and help your colleague to achieve the goal. Your target isn't that the employee fails to push down the salary raise in the next year. You want to motivate and enable your employee to his fullest potential. So create an environment where everyone knows if the goals were achieved before the next growth talk. Don't create unnecessary stress and surprises.

## Use Goals to Distribute your Responsibilities

Finding goals for the experienced colleagues can be one of the hardest challenges. The issues are that they are probably on a good technical level and do a good job overall. For these colleagues you can use well-defined goals to enable them to gain responsibilities that otherwise you would take over.

One of the more experienced employees of mine has the following goal:

> citation ""
> &lt;employee> will take on extensive responsibility for a critical project. For example, as a product owner, &lt;employee> will define a software component and lead its implementation with the help of a team.

In the past me and my team-lead colleagues took over the responsibility to pitch, plan and coordinate the development of a new software component that is shared between the projects. But wouldn't it be better if you would've staff in your team that can do that on their own with very little help and consulting of yours. This goal forces you and your employee to try that out. In the worse case the employee fails, and you just do what you would've done in the first place. You take the responsibility for the development yourself. But in the (more likely) best case you enabled someone else that is capable to coordinate and bring a project to success on its own. This colleague will also be enabled to take over your position when the time comes that you move to a different position. You make sure that there's always someone that can replace you.

## A Summary

This blog is a collection of random thoughts that I had when I defined the development goals for my employees in the past months. This topic is huge. Way larger, then what we discussed in this post. But the points I addressed are crucial in my opinion to enable your employees. You should not just define goals because you need to. You should use them to form better software engineers, to improve the skill set of your team and to motivate your staff to try something new and possibly uncomfortable they did not do before.
