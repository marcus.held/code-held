---
layout: post
title: Don't confuse configuration and constants
image: assets/images/posts/2022/11/configuration-constants.jpg
image-alt: An alarm
tags: [best practice, clean code]
highlight: false
call-to-action: Did this post help you? Tell me your experience
credits: Photo by <a href="https://unsplash.com/@mpho_mojapelo?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Mpho Mojapelo</a> on <a href="https://unsplash.com/s/photos/constant?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
---
From time to time I experience that an application is unnecessarily configurable. In a recent project I experienced how too many options lead me to feel much more complexity and increased my mental load. Looking deeper into some of the values I saw that these were actually constants that would be dangerous to change "on-the-fly" anyway. In this post I reflect on this and share my thoughts.

In this project the backend processes data from an embedded devices that communicate through [LPWA](https://en.wikipedia.org/wiki/Low-power_wide-area_network) networks. This sets specific constraints on the system. While we developed on that system the embedded developers figured out specific values for the best user experience in terms of e.g. timeout configurations. These values were intuitively implemented as spring properties, since we had to "play around" a bit in the beginning and the underlying library that we configured also called the properties a "configuration". But then it stayed in the project. And in the `application.properties`. When I joined the project this confused me. It created the impression, that these values are meant to be configurable. But actually this is not the case. For our system - with the specific constraints - these values are constants.  
I actually don't want to configure them without testing the application. Also, these values are in sync with our firmware. So I can't even change them without a coordinated effort. In these scenarios I rather want to have a separate deployment and force myself to go through the whole build-test-run cycle. 

So, what's the lesson here? Don't make everything configurable just because you can. You should offer configuration only when you need it. Remember [YAGNI](https://en.wikipedia.org/wiki/You_aren%27t_gonna_need_it), [KISS](https://en.wikipedia.org/wiki/KISS_principle) and [Murphy's law](https://en.wikipedia.org/wiki/Murphy%27s_law).
