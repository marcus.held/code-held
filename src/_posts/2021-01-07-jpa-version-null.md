---
layout: post 
title: JPA Doesn't Call Persist But Merge Operation On New Entities
image: assets/images/posts/2021/01/jpa-persist-header.jpg
image-alt: A HDD
tags: [JPA, Hibernate]
highlight: true 
call-to-action: Did this post help you? Tell me your experience 
credits: Photo by benjamin lehman on Unsplash
---
When working with JPA I prefer generating the primary key on the application and not in the database (check out [this post from James Brundege](https://web.archive.org/web/20170814122921/http://www.onjava.com/pub/a/onjava/2006/09/13/dont-let-hibernate-steal-your-identity.html)). Additionally, I also prefer optimistic locking and in order to use it you need to specify a {% ihighlight kotlin %}@Version{% endihighlight %} field in your Entity. But you need to be careful how to initialize these two fields. In this post I'll talk about an error when you assign a default value for both fields in JPA which leads the {% ihighlight kotlin %}EntityManager{% endihighlight %} to never call {% ihighlight kotlin %}persist{% endihighlight %} but the {% ihighlight kotlin %}merge{% endihighlight %} operation instead when creating a new entity.

> note "Source Code"
> You can find the example code on [GitHub](https://github.com/marcus-held/jpa-version-null)

Let's start with a simple definition of two entities with a one-to-many relationship:
{% highlight kotlin %}
@Entity
class ExampleEntity(
	@Id var id: UUID = UUID.randomUUID(),
	@Version var version: Long = 0L,
	@OneToMany var composite: List<Composite>
)

@Entity
class Composite(
	@Id var id: UUID = UUID.randomUUID(),
	@Version var version: Long = 0L
)
{% endhighlight %}

> note "Info"
> In a real project I'd prefer using an abstract mapped superclass to implement the basic functionality of creating the id and specifying the version field.

When trying to save a {% ihighlight kotlin %}ExampleEntity{% endihighlight %} with {% ihighlight kotlin %}repository.save(ExampleEntity(composite = listOf(Composite(), Composite()))){% endihighlight %} we get a {% ihighlight kotlin %}EntityNotFoundException{% endihighlight %}. This is because in a single transaction we want to _persist_ a {% ihighlight kotlin %}ExampleEntity{% endihighlight %} and all the {% ihighlight kotlin %}Composite{% endihighlight %} objects. But in our definition we didn't instruct JPA to _cascade_ the _persist_ operation. So - we can achieve that by adding it to the one-to-many relationship: {% ihighlight kotlin %}@OneToMany(cascade = [CascadeType.PERSIST]){% endihighlight %}. Save it. Run it. And ... It still crashes!?

And here comes the behavior I wasn't aware of. After looking into the SQL that got executed I noticed that Hibernate executed a {% ihighlight sql %}select{% endihighlight %} on the composite table. So for some reasons Hibernate expected that the composite entity already exists instead of creating it.

The reason why Hibernate is doing that is because as soon as an Entity has a non-null primary key, and a non-null version JPA defines the Entity as existent and fetches it from the database.

So the lesson is easy. **Don't - ever - initialize, touch or modify the version field!** A look into the [JPA specification](https://download.oracle.com/otndocs/jcp/persistence-2_1-fr-eval-spec/index.html) (Chapter 3.4.2) also clearly states:

> citation ""
> An entity may access the state of its version field or property or export a method for use by the application to access the version, but must not modify the version value

So, this is the correct implementation:

{% highlight kotlin %}
@Entity
class ExampleEntity(
@Id var id: UUID = UUID.randomUUID(),
@Version var version: Long? = null,
@OneToMany(cascade = [CascadeType.PERSIST]) var composite: List<Composite>
)

@Entity
class Composite(
@Id var id: UUID = UUID.randomUUID(),
@Version var version: Long? = null
)
{% endhighlight %}

You learn something new every day ;-)
