---
layout: post 
title: New Position and the Future of This Blog
image: assets/images/posts/2021/12/desk.jpg
image-alt: A person working on a desk
tags: []
highlight: false 
call-to-action: Do you want to work with me? Get in touch with me!
credits: Photo by <a href="https://unsplash.com/@zal3wa?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Damian Zaleski</a> on <a href="https://unsplash.com/s/photos/manager?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
---
Today I write an unusual post for this blog - A personal note. I recently took the responsibility of the System & Infrastructure department. As you can imagine taking over such a position comes along with a lot of work - especially when you didn't get rid of the old responsibilities yet. That's the main reason why I didn't write a blog in the past months. 

But what about the future of this blog? In my new responsibility I'm farther away from the actual technology and on this blog I especially focused on that. I don't know how it will turn out for me, but when I managed to get into the new role and sorted out how I want to approach things, I definitely plan to get closer again. Until then, I might write a blog once in a while. I don't know about what yet. And it might be about some management stuff as well - but time will show. This blog was and is a platform for me to sort things out for myself, organize my thoughts, and to make my argumentation's comprehensible. And it will remain like that.

Until then - I want to finish with the offer that **I'm always looking for motivated backend developers in the Spring ecosystem**. If you like what I write, live in Germany and would enjoy working with me - then get in touch with me at anytime. Write me a mail, on LinkedIn, on Twitter - wherever. I'm eager to talk with you!
