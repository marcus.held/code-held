---
layout: post
title: Jenkins Bitbucket Branch Source Plugin's Webhook Is Not Working
image: assets/images/posts/2019/05/jenkins-bitbucket.jpg
image-alt: Jenkins Artwork with Bitbucket Logo
tags: [Jenkins, Bitbucket, Git, Continuous Integration, DevOps]
highlight: false
call-to-action: Did this article help you? Tell me.
credits: Background Image by Isis França on Unsplash
---
In my current project we use Bitbucket Server as our Git backend and Jenkins with a pipeline multibranch project to build our project. For better integration of both technologies we decided to use the [Bitbucket Branch Source Plugin](https://wiki.jenkins.io/display/JENKINS/Bitbucket+Branch+Source+Plugin). This enables us to trigger automated builds and maintain dynamic jobs on our Jenkins. But installing the necessary webhooks turned out to not be a trivial task.

The goals I want to achieve are the following:
1. When a commit is pushed to a branch the respective jenkins job should be built.
1. When a pull request is opened the branch should be built
1. When a pull request is merged the target branch should be built
1. When a pull request is declined or closed the job should be deleted.

All of the above requirements can be achieved by using the [Bitbucket Branch Source Plugin](https://wiki.jenkins.io/display/JENKINS/Bitbucket+Branch+Source+Plugin) and installing a webhook in Bitbucket. There is a proper [documentation](https://support.cloudbees.com/hc/en-us/articles/115000053051-How-to-Trigger-Multibranch-Jobs-from-Bitbucket-Server-) on how to set that up on cloudbees support page, but there are quite some caveats to run into.

When setting it up like described I saw no issues in the event log of the webhook in Bitbucket but unfortunately the hooks seem to have no effect on the repspective jobs in Jenkins. So looking into the system log of Jenkins I also saw no log that where related to the webhooks, but I saw a log entry for the test ping that I did when setting up the webhooks. 

So I did some research and found another [troubleshooting page for Bitbucket Webhooks](https://support.cloudbees.com/hc/en-us/articles/115000051112#multibranch) in which it is described to add `com.cloudbees.jenkins.plugins.bitbucket.hooks` as a logger. So I did exactly that and saw logs like: 
```
X-Bitbucket-Type header / server_url request parameter not found. Bitbucket Cloud webhook incoming.
```
That is it! Their seems to be an undocumented parameter that needs to be set for the webhook that provides the URL of the Bitbucket Server. Knowing that I stumbled upon [this issue](https://issues.jenkins-ci.org/browse/JENKINS-55649) in Jenkins Jira that describes this bug and in the comments the solution is mentioned by Darin Patrick and Benjamin Brummer. You need to provide the parameter in the URL of the webhook like this:

```
https://jenkins.company.com/bitbucket-scmsource-hook/notify?server_url=https%3A%2F%2Fbitbucket.company.com
```

Pay attention that the `server_url` needs to be properly escaped. After changing the URL of the webhook everything worked as expected.
