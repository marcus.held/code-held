---
title: Legends of Honor
image: portfolio/legends-of-honor.jpg
links:
 - href: /assets/pdf/loh_factsheet.pdf
   text: Download Factsheet
 - href: https://www.goodgamestudios.com/games/legendsofhonor/
   text: Visit Project Page
---
<span class="portfolio-section-title">What is it?</span>

Legends of Honor is a massive multiplayer online strategy browser game. In the game you take control over a medieval kingdom and move through the world with your army in real time.

<span class="portfolio-section-title">What did I do?</span>

When the project was started I joined it right away as the first backend developer. In this role I had the technical responsibility to design the server architecture and lead a team of 10 backend developers until the launch of the project.
