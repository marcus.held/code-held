---
title: Rise of Cultures
image: portfolio/rise-of-cultures.jpg
links:
 - href: https://youtu.be/AX5V8ZdfjRs
   text: Watch Marketing Video
---
<span class="portfolio-section-title">What is it?</span>

Rise of Cultures is a simulation game where you guide your civilization through the ages, meet other cultures, conquer continents and built up your cities.

<span class="portfolio-section-title">What did I do?</span>

I developed and maintained the backend, which we developed with Java, Spring and Hibernate. When I worked on the project we released it for the first time on the US market to test early retention KPIs.
