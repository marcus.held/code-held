---
title: Goodgame Empire
image: portfolio/empire.jpg
links:
 - href: /assets/pdf/empire_factsheet.pdf
   text: Download Factsheet
 - href: https://www.goodgamestudios.com/games/goodgame-empire/
   text: Visit Project Page
---
<span class="portfolio-section-title">What is it?</span>

Goodgame Empire is a massive multiplayer browsergame with more than 70 million registered players. As a player you build up your castle to rule over four different kingdoms.

<span class="portfolio-section-title">What did I do?</span>

I started working on Empire 2014 and was one of the main backend developers of one of the two feature teams we operated. In this project we operated a Java based server which handled thousands of concurrent users with a high amount of requests per minute.
