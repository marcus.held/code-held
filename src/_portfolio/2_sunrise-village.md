---
title: Sunrise Village
image: portfolio/sunrise-village.png
links:
 - href: https://youtu.be/v82cXNKvfOI
   text: Watch Marketing Video
---
<span class="portfolio-section-title">What is it?</span>

Sunrise Village was a character driven simulation game in which the player built up a village and explored the world with his character. The game featured a rich exploration of the world and an extensive production simulation.

<span class="portfolio-section-title">What did I do?</span>

I developed and designed the server from the early days of production with Java, C#, Spring, Hibernate, RabbitMQ and .NET Core. One of the main features of the backend was a .NET Core application that used the same business logic as our client. The backend was capable to simulate multiple players moving on the same map.
