---
name: code-talks-2019
youtube-id: WjXyBqIG1Ec
thumb: assets/images/portfolio/talks/codetalks2019.jpg
title: Boost Your Development With Proper API Design
conference: code.talks 2019
links:
- href: https://youtu.be/WjXyBqIG1Ec
  text: Watch on youtube
- href: https://codetalks.de/
  text: codetalks.de
---
With 1600 attendees is code.talks the largest developer conference in Germany. Around 400 people listened to my talk where I discussed several aspect of robust software architecture design.
