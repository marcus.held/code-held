---
name: Sascha Boeing
link: https://www.xing.com/profile/Sascha_Boeing/cv
link-text: Xing
---
After studying computer science in Ravensburg Sascha worked several years on online games as a backend developer. Recently his focus is on developing custom software for Sport Alliance GmbH in Hamburg.
