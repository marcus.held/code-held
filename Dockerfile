FROM jekyll/jekyll:4.0.1

RUN apk add --update \
  python \
  python-dev \
  py-pip \
  build-base \
  libxml2-dev libxslt-dev libffi-dev gcc musl-dev libgcc openssl-dev curl \
  jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev \
  imagemagick graphviz \
  && pip install blockdiag seqdiag actdiag nwdiag

ENV BUNDLE_PATH=$GEM_HOME

WORKDIR /tmp

ADD src/Gemfile /tmp/
ADD src/Gemfile.lock /tmp/

RUN chown -R jekyll:jekyll /tmp
RUN chown -R jekyll:jekyll /usr/gem

RUN bundle install

WORKDIR /srv/jekyll
