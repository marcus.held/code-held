# Premonition
![Premonition](premonition.png)

Simple note with no header

```markdown
> note ""
> No headers in here
```

Note

```markdown
> note "I am a note"
> The body of the note goes here. Premonition allows you to write any `Markdown` inside the block.
```

Info

```markdown
> info "I am some info"
> The body of the info box goes here. Premonition allows you to write any `Markdown` inside the block.
```

Warning

```markdown
> warning "I am a warning"
> The body of the warning box goes here. Premonition allows you to write any `Markdown` inside the block.
```

Error

```markdown
> error "I am an error"
> The body of the error box goes here. Premonition allows you to write any `Markdown` inside the block.
```

Citation (Note the use of attributes here)

```markdown
> citation "Mark Twain" [ cite = "mt" ]
> I will be a beautiful citation quote
```
